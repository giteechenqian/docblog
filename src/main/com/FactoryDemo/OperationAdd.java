package FactoryDemo;

public class OperationAdd extends Operation{
    @Override
    public double getResult() {
        return getOperationA()+getOperationB();
    }
}
