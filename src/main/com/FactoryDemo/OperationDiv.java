package FactoryDemo;

public class OperationDiv extends Operation {
    @Override
    public double getResult()  {
        double a = getOperationA();
        double b = getOperationB();
        if(b == 0){
            try {
                throw new ArithmeticException("除数不能为0错误！");
            } catch (ArithmeticException e) {
                e.printStackTrace();
                System.out.println("请重新输入！");
            }
            System.out.println("bye!");
            return 0;
        }

        return a/b;
    }
}
