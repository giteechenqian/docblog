package FactoryDemo;

public class OperationSub extends Operation {
    @Override
    public double getResult() {
        return getOperationA() - getOperationB();
    }
}
