package FactoryDemo;

public abstract class Operation {
    private double operationA = 0;
    private double operationB = 0;

    public double getOperationA() {
        return operationA;
    }

    public void setOperationA(double operationA) {
        this.operationA = operationA;
    }

    public double getOperationB() {
        return operationB;
    }

    public void setOperationB(double operationB) {
        this.operationB = operationB;
    }

    public abstract double getResult() throws Exception;
}
