package FactoryDemo;

public class Test {
    public static void main(String[] args) throws Exception {
        //System.out.println("test!");
        Operation oper = OperationFactory.creatOperation('/');
        oper.setOperationA(3);
        oper.setOperationB(1);
        double res = oper.getResult();
        System.out.println("res="+res);
    }
}
