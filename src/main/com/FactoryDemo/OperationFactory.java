package FactoryDemo;
public class OperationFactory {
    public static Operation creatOperation(char operate){
        Operation oper = null;
        switch (operate){
            case '+':
                oper = new OperationAdd();
                break;
            case '-':
                oper = new OperationSub();
                break;
            case '*':
                oper = new OperationMul();
                break;
            case '/':
                oper = new OperationDiv();
                break;
                default:
                    throw new RuntimeException("请输入正确操作符");
        }
        return oper;
    }
}
