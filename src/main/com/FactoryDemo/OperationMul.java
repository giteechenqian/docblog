package FactoryDemo;

public class OperationMul extends  Operation {
    @Override
    public double getResult() {
        return getOperationA() * getOperationB();
    }
}
